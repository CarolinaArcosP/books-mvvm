//
//  CommentCellViewModel.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/10/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result

class CommentCellViewModel {
    private let _comment: Comment
    private let _imageMutable = MutableProperty(UIImage.emptyBookImage)
    let imageProperty: Property<UIImage>
    
    // MARK: - View configuration texts
    var name: String {
        return _comment.user.name.capitalized
    }
    var comment: String {
        return _comment.comment
    }
    
    init(comment: Comment) {
        _comment = comment
        imageProperty = Property(capturing: _imageMutable)
        fetchImage()
    }
    
    // MARK: - Private methods
    private func fetchImage() {
        if let imageURL = _comment.user.imageURL {
            let imageFetcher = ImageFetcher()
            imageFetcher.fetchImage(imageURL)
                .take(duringLifetimeOf: self)
                .startWithResult { result in
                    switch result {
                    case .success(let image):
                        self._imageMutable.value = image
                    case .failure: self._imageMutable.value = .emptyBookImage
                    }
            }
        }
    }
}
