//
//  CommentCell.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/10/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore
import ReactiveSwift
import Result

class CommentCell: UITableViewCell, NibLoadable {
    
    // MARK: - Properties
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    
    // MARK: - Helper methods
    
    func configureCell(with viewModel: CommentCellViewModel) {
        nameLabel.text = viewModel.name
        commentLabel.text = viewModel.comment
        userImageView.reactive.image <~ viewModel.imageProperty
    }
}
