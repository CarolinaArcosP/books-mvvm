//
//  BookCommentsController.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore

class BookCommentsController: UIViewController {
    // MARK: - Properties
    private lazy var _view: BookCommentsView = BookCommentsView.loadFromNib()!
    private let _commentsViewModel: BookCommentsViewModel
    let estimatedRowHeight: CGFloat = 90
    
    // MARK: - Initializers
    init(commentsViewModel: BookCommentsViewModel) {
        _commentsViewModel = commentsViewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = _view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureTableView(_view.tableView)
        bindViewModel(_commentsViewModel)
    }
    
    // MARK: - Private methods
    
    private func configureTableView(_ tableView: UITableView) {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = estimatedRowHeight
        tableView.register(cell: CommentCell.self)
        tableView.register(cell: EmptyCommentCell.self)
    }
    
    private func bindViewModel(_ viewModel: BookCommentsViewModel) {
        viewModel.comments.producer
            .take(during: self.reactive.lifetime)
            .startWithValues { [unowned self] _ in
                self._view.tableView.reloadData()
        }
    }
}

// MARK: - UITableViewDelegate and UITableViewDataSource
extension BookCommentsController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _commentsViewModel.comments.value.isEmpty ? 1 : _commentsViewModel.comments.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard _commentsViewModel.comments.value.isNotEmpty else {
            let cell = _view.tableView.dequeue(cell: EmptyCommentCell.self)!
            return cell
        }

        let cell = _view.tableView.dequeue(cell: CommentCell.self)!
        let viewModel = CommentCellViewModel(comment: _commentsViewModel.comments.value[indexPath.row])
        cell.configureCell(with: viewModel)
        return cell
    }

}
