//
//  BookCommentsViewModel.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import ReactiveSwift
import Networking
import Result

class BookCommentsViewModel {
    // MARK: - Properties
    private let _book: Book
    private let _booksRepository: BooksRepositoryType
    private let _mutableComments = MutableProperty([Comment]())
    let comments: Property<[Comment]>
    
    // MARK: - Initializers
    init(book: Book, booksRepository: BooksRepositoryType) {
        _book = book
        _booksRepository = booksRepository
        comments = Property(_mutableComments)
        getBookComments()
    }
    
    // MARK: - Public methods
    func getBookComments() {
        self._booksRepository.getBookComments(id: _book.id)
            .startWithResult({ (result) in
                switch result {
                case .success(let comments):
                    self._mutableComments.value = comments
                case .failure:
                    print("Comments error")
                }
            })
    }
}
