//
//  BookDetailsViewModel.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation

class BookDetailsViewModel {
    
    // MARK: - Properties
    
    private let _book: Book
    private let _userRepository: UserRepositoryType
    private let _bookRepository: BooksRepositoryType
    
    // MARK: - Initializers
    
    init(book: Book, userRepository: UserRepositoryType, bookRepository: BooksRepositoryType) {
        _book = book
        _userRepository = userRepository
        _bookRepository = bookRepository
    }
    
    func createBookInformatonViewModel() -> BookInformationViewModel {
        return BookInformationViewModel(book: _book, userRepository: _userRepository)
    }
    
    func createBookCommentsViewModel() -> BookCommentsViewModel {
        return BookCommentsViewModel(book: _book, booksRepository: _bookRepository)
    }
}
