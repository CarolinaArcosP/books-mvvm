//
//  BookInformationViewModel.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import ReactiveSwift
import Result
import Networking

class BookInformationViewModel {
    
    // MARK: - Properties
    private let _book: Book
    private let _userRepository: UserRepositoryType
    private let _imageMutable = MutableProperty(UIImage.emptyBookImage)
    private let _mutableBookStatus: MutableProperty<BookStatus>
    let imageProperty: Property<UIImage>
    let bookStatus: Property<BookStatus>
    private(set) lazy var rentAction = Action<(), Rent, RepositoryError>(enabledIf: bookStatus.map { $0 == BookStatus.available }) { _ in
        self.rentBook()
    }
    
    // MARK: - View configuration texts
    var title: String {
        return _book.title
    }
    
    var author: String {
        return _book.author
    }
    
    var releaseYear: String {
        return _book.releaseYear
    }
    
    var genre: String {
        return _book.genre.capitalized
    }
    
    // MARK: - Initializer
    init(book: Book, userRepository: UserRepositoryType) {
        _book = book
        _userRepository = userRepository
        imageProperty = Property(_imageMutable)
        _mutableBookStatus = MutableProperty(_book.status)
        bookStatus = Property(_mutableBookStatus)
        fetchImage()
        
        bookStatus.producer.startWithValues { [unowned self] status in
            self._book.status = status
        }
    }
    
    // MARK: - Private methods
    private func fetchImage() {
        if let imageURL = _book.imageUrl {
            let imageFetcher = ImageFetcher()
            imageFetcher.fetchImage(imageURL)
                .take(duringLifetimeOf: self)
                .startWithResult { result in
                    switch result {
                    case .success(let image):
                        self._imageMutable.value = image
                    case .failure: self._imageMutable.value = .emptyBookImage
                    }
            }
        }
    }
    
    // MARK: - Public methods
    func rentBook() -> SignalProducer<Rent, RepositoryError> {
        return self._userRepository.rentBook(id: _book.id).on(value: { [weak self] _ in
            self?._mutableBookStatus.value = BookStatus.notAvailable
        })
    }
    
    func isRentAvailable(for status: BookStatus) -> Bool {
        return status == BookStatus.available
    }
}
