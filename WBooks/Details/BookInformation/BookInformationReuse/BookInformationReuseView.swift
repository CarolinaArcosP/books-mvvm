//
//  BookInformationReuseView.swift
//  WBooks
//
//  Created by Carolina Arcos on 3/27/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import WolmoCore
import ReactiveSwift
import Result

class BookInformationReuseView: UIView, NibLoadable {
    
    // MARK: - Contants
    
    struct Constants {
        static let buttonCornerRadius: CGFloat = 20
        static let buttonBorderWidth: CGFloat = 1
    }
    
    // MARK: - Properties
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = Constants.buttonCornerRadius
        }
    }
    @IBOutlet weak var bookCoverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var addToWishlistButton: UIButton! {
        didSet {
            addToWishlistButton.layer.borderColor = UIColor.applicationBlueColor.cgColor
            addToWishlistButton.layer.cornerRadius = Constants.buttonCornerRadius
            addToWishlistButton.layer.borderWidth = Constants.buttonBorderWidth
        }
    }
    @IBOutlet weak var rentButton: UIButton! {
        didSet {
            rentButton.layer.cornerRadius = Constants.buttonCornerRadius
        }
    }
    
    deinit {
        rentButton.gradient = .none
    }
    
    // MARK: - Helper methods
    func configureRentButton(isEnabled: Bool) {
        rentButton.isEnabled = isEnabled
        rentButton.setGradientBackground(enabled: isEnabled)
    }
    
    func setStatusTo(_ status: BookStatus) {
        statusLabel.text = status.rawValue
        statusLabel.textColor = status.textColor
    }
}
