//
//  BookInformationReuseViewController.swift
//  WBooks
//
//  Created by Carolina Arcos on 3/27/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class BookInformationReuseViewController: UIViewController {
    
    // MARK: - Constants
    struct Constants {
        static let bookAvailableMessage = "This book is available, you can rent it"
        static let rentUnavailableMessage = "Sorry, this book is not available"
    }
    
    // MARK: - Properties
    private lazy var _view: BookInformationReuseView = BookInformationReuseView.loadFromNib()!
    private let _viewModel: BookInformationViewModel
    
    // MARK: - Initiaizers
    init(bookViewModel: BookInformationViewModel) {
        _viewModel = bookViewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = _view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindRentButtonAction()
        configureView()
    }
    
    // MARK: - Private methods
    private func configureView() {
        _view.titleLabel.text = _viewModel.title
        _view.authorLabel.text = _viewModel.author
        _view.bookCoverImageView.reactive.image <~ _viewModel.imageProperty
    }
    
    private func bindRentButtonAction() {
        _view.rentButton.reactive.pressed = CocoaAction(_viewModel.rentAction)
        
        _viewModel.bookStatus.producer
            .take(during: self.reactive.lifetime)
            .startWithValues { [weak self] status in
                guard let self = self else { return }
                let isRentAvailable = self._viewModel.isRentAvailable(for: status)
                self._view.setStatusTo(status)
                self._view.configureRentButton(isEnabled: isRentAvailable)
                let message = isRentAvailable ? Constants.bookAvailableMessage : Constants.rentUnavailableMessage
                self.showMessage(message)
        }
    }
    
    private func showMessage(_ text: String) {
        let alert = UIAlertController(title: "", message: text, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 2.5, repeats: false, block: { _ in alert.dismiss(animated: true, completion: nil) })
    }
}
