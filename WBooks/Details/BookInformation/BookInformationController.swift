//
//  BookInformationController.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class BookInformationViewController: UIViewController {
    // MARK: - Constants
    struct Constants {
        static let rentBookSuccessMessage = "Book rented"
        static let rentBookFailureMessage = "The book could not be rented. Try again"
    }
    
    // MARK: - Properties
    private lazy var _view: BookInformationView = BookInformationView.loadFromNib()!
    private let _viewModel: BookInformationViewModel
    
    // MARK: - Initiaizers
    init(bookViewModel: BookInformationViewModel) {
        _viewModel = bookViewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = _view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        bindRentButtonAction()
        configureView()
    }
    
    // MARK: - Private methods
    private func configureView() {
        _view.titleLabel.text = _viewModel.title
        _view.authorLabel.text = _viewModel.author
        _view.releaseYearLabel.text = _viewModel.releaseYear
        _view.genreLabel.text = _viewModel.genre
        _view.bookCoverImageView.reactive.image <~ _viewModel.imageProperty
    }
    
    private func bindRentButtonAction() {
        _view.rentButton.reactive.pressed = CocoaAction(_viewModel.rentAction)
        _viewModel.rentAction.values.take(during: reactive.lifetime).observeValues { _ in
            self.showMessage(Constants.rentBookSuccessMessage)
        }
        
        _viewModel.rentAction.errors.take(during: reactive.lifetime).observeValues { _ in
            self.showMessage(Constants.rentBookFailureMessage)
        }
        
        _viewModel.bookStatus.producer
            .take(during: self.reactive.lifetime)
            .startWithValues { [weak self] status in
                guard let self = self else { return }
                self._view.setStatusTo(status)
                self._view.configureRentButton(isEnabled: self._viewModel.isRentAvailable(for: status))
        }
    }
    
    private func showMessage(_ text: String) {
        let alert = UIAlertController(title: "", message: text, preferredStyle: .alert)
        self.present(alert, animated: true, completion: nil)
        Timer.scheduledTimer(withTimeInterval: 1.5, repeats: false, block: { _ in alert.dismiss(animated: true, completion: nil) })
    }
}
