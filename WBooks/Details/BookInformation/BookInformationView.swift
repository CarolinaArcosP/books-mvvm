//
//  BookInformationView.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import WolmoCore
import ReactiveSwift
import Result

class BookInformationView: UIView, NibLoadable {
    
    // MARK: - Contants
    let buttonCornerRadius: CGFloat = 20
    let buttonBorderWidth: CGFloat = 1
    
    // MARK: - Properties    
    @IBOutlet weak var containerView: UIView! {
        didSet {
            containerView.layer.cornerRadius = buttonCornerRadius
        }
    }
    @IBOutlet weak var bookCoverImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var releaseYearLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var addToWishlistButton: UIButton! {
        didSet {
            addToWishlistButton.layer.borderColor = UIColor.applicationBlueColor.cgColor
            addToWishlistButton.layer.cornerRadius = buttonCornerRadius
            addToWishlistButton.layer.borderWidth = buttonBorderWidth
        }
    }
    @IBOutlet weak var rentButton: UIButton! {
        didSet {
            rentButton.layer.cornerRadius = buttonCornerRadius
        }
    }
    
    deinit {
        rentButton.gradient = .none
    }
    
    // MARK: - Helper methods
    func configureRentButton(isEnabled: Bool) {
        rentButton.isEnabled = isEnabled
        rentButton.setGradientBackground(enabled: isEnabled)
    }
    
    func setStatusTo(_ status: BookStatus) {
        statusLabel.text = status.rawValue
        statusLabel.textColor = status.textColor
    }
}
