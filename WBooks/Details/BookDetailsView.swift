//
//  BookDetails.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore

class BookDetailsView: UIView, NibLoadable {
    @IBOutlet weak var informationContainerView: UIView!
    @IBOutlet weak var commentsContainerView: UIView!
}
