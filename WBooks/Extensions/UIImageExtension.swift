//
//  UIImageExtension.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/31/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit

extension UIImage {
    static let libraryIcon: UIImage =  UIImage(imageLiteralResourceName: "ic_library")
    static let libraryActiveIcon: UIImage = UIImage(imageLiteralResourceName: "ic_library_active")
    static let emptyBookImage: UIImage = UIImage(imageLiteralResourceName: "ic_profile_picture")
    static let nevigationBarImage: UIImage  =  UIImage(imageLiteralResourceName: "bc_nav bar")
}
