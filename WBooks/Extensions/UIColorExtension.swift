//
//  UIColorExtension.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/6/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit

extension UIColor {
    static let applicationBlueColor = UIColor(hex: "00ADEE")!
    static let greenAvailable = UIColor(hex: "A5CD39")!
    static let redUnavailable = UIColor(hex: "D0021B")!
}
