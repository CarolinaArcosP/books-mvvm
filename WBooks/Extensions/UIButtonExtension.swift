//
//  UIButtonExtension.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/6/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore

extension UIButton {
    
    // MARK: - Constants
    struct Constants {
        static let enabledColors = [UIColor(hex: "00ADEE")!, UIColor(hex: "38CCCD")!]
        static let disabledColors = [UIColor(hex: "EAEAEA")!, UIColor(hex: "F4F4F4")!]
    }
    
    func setGradientBackground(enabled: Bool) {
        let colors: [UIColor]
        if enabled {
            colors = Constants.enabledColors
        } else {
            colors = Constants.disabledColors
        }
        
        self.gradient = ViewGradient(colors: colors, direction: .leftToRight)
    }
}
