//
//  LibraryTableView.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/30/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore

class LibraryTableView: UIView, NibLoadable {
    @IBOutlet weak var tableView: UITableView!
}
