//
//  LibraryViewController.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/30/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore
import ReactiveSwift
import ReactiveCocoa

class LibraryViewController: UIViewController {
    // MARK: - Properties
    private lazy var _view: LibraryTableView = LibraryTableView.loadFromNib()!
    private let _booksViewModel: LibraryViewModel
    let estimatedRowHeight: CGFloat = 90
    
    // MARK: - Initializers
    init(booksViewModel: LibraryViewModel) {
        _booksViewModel = booksViewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        self.view = _view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        automaticallyAdjustsScrollViewInsets = false
        configureTableView(_view.tableView)
        setNavigationBarStyle()
        bindViewModel()
    }
    
    // MARK: - Private methods
    
    private func configureTableView(_ tableView: UITableView) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = estimatedRowHeight
        tableView.register(cell: BookTableViewCell.self)
    }
    
    private func setNavigationBarStyle() {
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.barStyle = .black
        title = "Library"
    }
    
    private func bindViewModel() {
        _booksViewModel.books.producer
            .take(during: self.reactive.lifetime)
            .startWithValues { [unowned self] _ in
                self._view.tableView.reloadData()
        }
    }
}

// MARK: - Delegate and DataSource implementation
extension LibraryViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return _booksViewModel.books.value.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = _view.tableView.dequeue(cell: BookTableViewCell.self)!
        let viewModel = _booksViewModel.createBookCellViewModelFor(index: indexPath.row)
        viewModel.fetchImage()
        cell.configureCell(with: viewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        _view.tableView.deselectRow(at: indexPath, animated: true)
        let viewModel =  _booksViewModel.createBookDetailsViewModelFor(index: indexPath.row)
        let controller = BookDetailsViewController(bookViewModel: viewModel)
        navigationController!.pushViewController(controller, animated: true)
    }
}



















//func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    _view.tableView.deselectRow(at: indexPath, animated: true)
//    let controller: UIViewController
//    if indexPath.row % 2 == 0 {
//        let viewModel =  _booksViewModel.createBookDetailsViewModelFor(index: indexPath.row)
//        controller = BookDetailsViewController(bookViewModel: viewModel)
//    } else {
//        let viewModel = _booksViewModel.createBookInformationViewModelFor(index: indexPath.row)
//        controller = BookInformationReuseViewController(bookViewModel: viewModel)
//    }
//
//    navigationController!.pushViewController(controller, animated: true)
//}
