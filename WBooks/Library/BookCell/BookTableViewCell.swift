//
//  BookTableViewCell.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/30/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import WolmoCore
import ReactiveSwift
import Result

class BookTableViewCell: UITableViewCell, NibLoadable {
    // MARK: - Properties
    @IBOutlet weak var bookCoverImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    
    // MARK: - Helper methods
    func configureCell(with viewModel: BookCellViewModel) {
        nameLabel.text = viewModel.title
        authorLabel.text = viewModel.author
        bookCoverImageView.reactive.image <~ viewModel.imageProperty
    }
}
