//
//  BookCellViewModel.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/7/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import ReactiveSwift

class BookCellViewModel {
    private let _book: Book
    private let _imageMutable: MutableProperty<UIImage> = MutableProperty(UIImage.emptyBookImage)
    let imageProperty: Property<UIImage>
    
    init(book: Book) {
        _book = book
        imageProperty =  Property(capturing: _imageMutable)
    }
    
    // MARK: - View configuration texts
    var title: String {
        return _book.title
    }
    var author: String {
        return _book.author
    }
    
    func fetchImage() {
        if let imageURL = _book.imageUrl {
            let imageFetcher = ImageFetcher()
            imageFetcher.fetchImage(imageURL)
                .take(duringLifetimeOf: self)
                .startWithResult { result in
                    switch result {
                    case .success(let image):
                        self._imageMutable.value = image
                    case .failure:
                        self._imageMutable.value = .emptyBookImage
                    }
            }
        }
    }
}
