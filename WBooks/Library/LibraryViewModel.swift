//
//  LibraryViewModel.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/30/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import ReactiveSwift
import Result

class LibraryViewModel {
    let books: Property<[Book]>
    private let _mutableBooks = MutableProperty([Book]())
    private let _booksRepository: BooksRepositoryType
    private let _userRepository: UserRepositoryType

    // MARK: - Initializers
    init(booksRepository: BooksRepositoryType, userRepository: UserRepositoryType) {
        _booksRepository = booksRepository
        _userRepository = userRepository
        books = Property(_mutableBooks)
        updateBooks()
    }
    
    func updateBooks() {
        _booksRepository.getBooks().startWithResult { result in
            switch result {
            case .success(let booksArray):
                self._mutableBooks.value = booksArray
                print("success")
            case .failure:
                print("error")
            }
        }
    }
    
    func createBookDetailsViewModelFor(index: Int) -> BookDetailsViewModel {
        return BookDetailsViewModel(book: books.value[index], userRepository: _userRepository, bookRepository: _booksRepository)
    }
    
    func createBookInformationViewModelFor(index: Int) -> BookInformationViewModel {
        return BookInformationViewModel(book: books.value[index], userRepository: _userRepository)
    }
    
    func createBookCellViewModelFor(index: Int) -> BookCellViewModel {
        return BookCellViewModel(book: books.value[index])
    }
}
