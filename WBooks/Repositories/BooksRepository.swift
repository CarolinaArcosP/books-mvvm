//
//  BooksRepository.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/31/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import ReactiveSwift
import Networking
import Argo

protocol BooksRepositoryType {
    func getBooks() -> SignalProducer<[Book], RepositoryError>
    func getBookComments(id: Int) -> SignalProducer<[Comment], RepositoryError>
}

class BooksRepository: AbstractRepository, BooksRepositoryType {
    private static let fetchBooksPath = "/books"

    func getBooks() -> SignalProducer<[Book], RepositoryError> {
        let path = BooksRepository.fetchBooksPath
        
        return performRequest(method: .get, path: path) { json in
            return decode(json).toResult()
        }
    }
    
    func getBookComments(id: Int) -> SignalProducer<[Comment], RepositoryError> {
        let path = BooksRepository.fetchBooksPath + "/\(id)/comments"
        return performRequest(method: .get, path: path) { decode($0).toResult() }
    }
}
