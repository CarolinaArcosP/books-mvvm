//
//  UserRepository.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import ReactiveSwift
import Networking
import Argo

protocol UserRepositoryType {
    func rentBook(id: Int) -> SignalProducer<Rent, RepositoryError>
}

class UserRepository: AbstractRepository, UserRepositoryType {
    private static let rentPath = "users/51/rents"
    
    // MARK: - Consuming methods
    
    func rentBook(id: Int) -> SignalProducer<Rent, RepositoryError> {
        let path = UserRepository.rentPath
        let params: [String: Any]  = ["userID": 51,
                                      "bookID": id,
                                      "from": "2018-09-17",
                                      "to": "2018-09-17"]
        return performRequest(method: .post, path: path, parameters: params) { json in
            return decode(json).toResult()
        }
    }
}
