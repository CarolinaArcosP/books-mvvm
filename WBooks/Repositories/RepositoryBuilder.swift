//
//  RepositoryBuilder.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/31/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import Networking

public class RepositoryBuilder {
    static var DefaultNetworkingConfiguration: NetworkingConfiguration {
        var config = NetworkingConfiguration()
        config.domainURL = "swift-training-backend.herokuapp.com"
        return config
    }
    
    static func getDefaultBookRepository() -> BooksRepository {
        return BooksRepository(configuration: RepositoryBuilder.DefaultNetworkingConfiguration,
                              defaultHeaders: ["Content-Type": "application/json",
                                               "Accept": "application/json"])
    }
    
    static func getDefaultUserRepository() -> UserRepository {
        return UserRepository(configuration: RepositoryBuilder.DefaultNetworkingConfiguration,
                               defaultHeaders: ["Content-Type": "application/json",
                                                "Accept": "application/json"])
    }
}
