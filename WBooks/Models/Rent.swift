//
//  Rent.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import Foundation
import Argo
import Runes
import Curry

struct Rent {
    let id: Int
    let startDate: String
    let finalDate: String
    let bookId: Int
    let userId: Int
}

extension Rent: Argo.Decodable {
    public static func decode(_ json: JSON) -> Decoded<Rent> {
        return curry(Rent.init)
            <^> json <| "id"
            <*> json <| "from"
            <*> json <| "to"
            <*> json <| "bookID"
            <*> json <| "userID"
    }
}
