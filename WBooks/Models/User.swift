//
//  User.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/3/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import Argo
import Curry
import Runes

struct User {
    let id: Int
    let name: String
    let imageURL: URL?
    
    init(id: Int, name: String, imageURL: URL?) {
        self.id = id
        self.name = name
        self.imageURL = imageURL
    }
}

extension User: Argo.Decodable {
    static func decode(_ json: JSON) -> Decoded<User> {
        return curry(User.init)
            <^> json <| "id"
            <*> json <| "username"
            <*> ((json <|? "image") >>- toURL)
    }
}
