//
//  Book.swift
//  WBooks
//
//  Created by Carolina Arcos on 1/30/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import Argo
import Runes
import Curry

class Book {
    // MARK: - Properties
    let id: Int
    let author: String
    let title: String
    let imageUrl: URL?
    let releaseYear: String
    let genre: String
    var status: BookStatus
    
    init(id: Int, author: String, title: String, imageUrl: URL?, releaseYear: String, genre: String ) {
        self.id = id
        self.author = author
        self.title = title
        self.imageUrl = imageUrl
        self.releaseYear = releaseYear
        self.genre = genre
        status = Int.random(in: 0...1) == 0 ? BookStatus.available : BookStatus.notAvailable
    }
}

extension Book: Argo.Decodable {
    public static func decode(_ json: JSON) -> Decoded<Book> {
        return curry(Book.init)
            <^> json <| "id"
            <*> json <| "author"
            <*> json <| "title"
            <*> ((json <|? "image") >>- toURL)
            <*> json <| "year"
            <*> json <| "genre"
    }
}

enum BookStatus: String, Decodable {
    case available = "Available"
    case notAvailable = "Unavailable"
    
    var textColor: UIColor {
        switch self {
        case .available:
            return .greenAvailable
        case .notAvailable:
            return .redUnavailable
        }
    }
}
