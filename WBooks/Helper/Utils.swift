//
//  Utils.swift
//  WBooks
//
//  Created by Carolina Arcos on 2/10/20.
//  Copyright © 2020 Wolox. All rights reserved.
//

import UIKit
import Argo

func toURL(urlString: String?) -> Decoded<URL?> {
    return Decoded<URL?>.fromOptional(URL(string: urlString ?? ""))
}

func toBookStatus(statusString: String?)  -> Decoded<BookStatus?> {
    return Decoded<BookStatus>.fromOptional(BookStatus(rawValue: statusString ?? "Unavailable"))
}
